# Criação de um ambiente local com Docker, Postgres e PgAdmin4
![alt text](https://miro.medium.com/max/700/1*C27c2zlHixp40boXTiEqTw.png)

## Docker images
- Utilizaremos duas imagens oficiais do docker hub (https://hub.docker.com/)
- imagens:
    - https://hub.docker.com/r/dpage/pgadmin4/
    - https://hub.docker.com/_/postgres

## Docker compose
- É uma ferramenta que nos permite definir e executar vários containers docker em conjunto.
- Link para a documentação: https://docs.docker.com/compose/

## O que é feito no nosso arquivo docker-compose.yml

### Criação da network (rede de comunicação) 
- Basicamente definimos a criação de uma rede que irá se comunicar através de uma bridge (ponte).
- Bridge é um dispositivo de rede que atua nas camadas 1 e 2 do modelo OSI. Ela nos permitire realizar a comunicação entre segmentos de rede diferentes.
- Dessa forma um container poderá acessar o outro através do nome do container.

```
networks:
  postgres-network:
    driver: bridge
```

### Criação do nosso banco de dados local
- Primeiro é importante você definir um diretório local para configurar seu volume docker.
    - Exemplo: /home/$username/postgres/db:/var/lib/postgresql/data
- Feito isso mapearemos esse diretório no nosso arquivo docker-compose.
- Criaremos um container de nome "pg1".
- Para criação desse container utilizaremos a imagem postgres apresentada acima.
- Como não definimos o POSTGRES_USER, ele criará com o usuário padrão: postgres.
- Definimos uma senha e a porta.
- Definimos, também, a rede que o container irá pertencer.
```
pg1:
    image: postgres
    environment:
      POSTGRES_PASSWORD: "p0stgr35!"
    ports:
      - "5432:5432"
    volumes:
      - /home/$username/postgres/db:/var/lib/postgresql/data
    networks:
      - postgres-network
``` 

### Criação da nossa ferramenta de gerenciamento para o postgres - PgAdmin4
- Criaremos um container de nome "pgadmin1".
- Para criação desse container utilizaremos a imagem dpage/pgadmin4 apresentada acima.
- Definimos um usuário e senha e a porta.
- Definimos, também, a rede que o container irá pertencer. A mesma do container postgres.
```
pgadmin1:
    image: dpage/pgadmin4
    environment:
      PGADMIN_DEFAULT_EMAIL: "user@domain.com"
      PGADMIN_DEFAULT_PASSWORD: "5up3r53cret!"
    ports:
      - "8081:80"
    depends_on:
      - pg1
    networks:
      - postgres-network
``` 

### Faça o git pull do projeto ou copie o arquivo docker-compose.yml
- https://gitlab.com/docker73/dockernetworkpg/-/blob/master/docker-compose.yml
- Execute: sudo docker-compose up -d

### Verifique a criação dos containers
- docker ps 

### Acesse o pgadmin
- http://localhost:8081

OBS: Talvez ele demore um pouco para iniciar

### Acesse - Login
```
email: user@domain.com
password: 5up3r53cret!
```

### Configure seu banco de dados
1. Na aba superior clique em Object -> create -> server
2. General
```
Adicione um nome: meu_bd
```
3. Connection
```
  - Host: pg1
  - Port: 5432
  - Maintenance database: postgres
  - Username: postgres
  - Password: p0stgr35!
```
4. Save.

### Pronto.
- Agora só utilizar seu ambiente.


> 
